# USMON Chat Download Page Example
> A demo and simple implementation of a download page as well as a git and Gitlab playground with some real, relevant assests.

## Installation

### Requriements
-None

### Clone
 -Clone this repo to your local machine using `https://gitlab.com/IAmHollywood/usmon-chat-download.git`

## Built With
- HTML
- CSS
- Gitlab

## License

This project is licensed under the MIT license - see the [LICENSE.md](LICENSE.md) file for details
